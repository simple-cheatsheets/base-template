# Base Template for Cheatsheets

This is a simple template for cheatsheets and was created during my computer science studies.

For a more simple usage it also provides the functionality to use it inside a prepared docker image.

## Usage inside Docker

A complete setup is provided as a docker image. Therefore you can run the following command. This will provide a shell inside a docker container and allows you to run the usual latex commands.
```bash
docker run -it -v $(pwd):/home/sandbox registry.gitlab.com/simple-cheatsheets/base-template /bin/sh
```

In case your document is called `document.tex` executing this commands will generate a pdf document. The argument `--shell-escape` is required for working syntax-highlighting.

```bash
pdflatex --shell-escape document.tex
biber document
pdflatex --shell-escape document.tex
pdflatex --shell-escape document.tex
pdflatex --shell-escape document.tex
```

## Usage using MiKTeX (Windows)

For using this template on Windows clone or download the repo to your local pc. Afterwards configure a user defined TEXMF root directory using the MiKTeX Console. A possible setup is showen in the folowing image.

![Possible Configuration](https://gitlab.com/simple-cheatsheets/base-template/raw/master/docs/images/miktex-console_7kjXopEqVv.png "Possible Configuration")
